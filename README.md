# localstack

Kick start for mock of aws infra using `localstack`.

Pre-requisite:

	- awscli
	- openjdk8
	- localstack cli(python3 -m pip install localstack)
	- maven
	- docker (since I am going to use docker instead installing directly on host)

### You require aws configuration

```
aws configure set aws_access_key_id dummy
aws configure set aws_secret_access_key dummy
aws configure set default.region us-west-2
```

### Run localstack first

`localstack start`

### Running code by going to run directory

```       
sudo mvn clean install 
sudo mvn test
```

For professional need, contact me at:

shad.hasan@tinyorb.xyz
